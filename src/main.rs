use nannou::prelude::*;
use nannou::rand::rand;

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    position: Point2,
    next_position: Point2,
    color: Rgb<f32>,
}

fn model(app: &App) -> Model {
    let width = 600;
    let height = 600;
    app.new_window().size(width, height).view(view).build().unwrap();

    let position = pt2(0.0, 0.0);
    let next_position = pt2(0.0, 0.0);
    let color = rgb(0.5, 0.5, 0.5);

    Model { position, next_position, color }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let rect = app.main_window().rect();
    model.position = model.next_position;
    model.next_position = new_position(rect, model.position);
    model.color = new_color(model.color);
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.line()
        .start(model.position)
        .end(model.next_position)
        .weight(1.0)
        .color(model.color);
    draw.to_frame(app, &frame).unwrap();
}

fn new_position(rect: Rect, old_position: Point2) -> Point2 {
    let x_change = rand::random::<f32>() * 20.0 - 10.0;
    let y_change = rand::random::<f32>() * 20.0 - 10.0;
    let mut x = old_position[0] + x_change;
    if x > rect.right() || x < rect.left() {
        x = old_position[0] - x_change;
    }
    let mut y = old_position[1] + y_change;
    if y > rect.top() || y < rect.bottom() {
        y = old_position[1] - y_change;
    }

    pt2(x, y)
}

fn new_color(old_color: Rgb<f32>) -> Rgb<f32> {
    let red_change = rand::random::<f32>() * 0.02 - 0.01;
    let green_change = rand::random::<f32>() * 0.02 - 0.01;
    let blue_change = rand::random::<f32>() * 0.02 - 0.01;
    let mut red = old_color.red + red_change;
    if red > 1.0 || red < 0.0 {
        red = old_color.red + red_change;
    }
    let mut green = old_color.green + green_change;
    if green > 1.0 || green < 0.0 {
        green = old_color.green + green_change;
    }
    let mut blue = old_color.blue + blue_change;
    if blue > 1.0 || blue < 0.0 {
        blue = old_color.blue + blue_change;
    }

    rgb(red, green, blue)
}
